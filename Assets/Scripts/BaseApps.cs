﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

namespace BaseApps
{
    public class JSONEvent : UnityEvent<string> { }

    public struct URL
    {
        public const string SERVER =
                                    "https://api.bojonggededeveloper.com/ngepasin/";
                                    //"http://192.168.100.2/ngepasin/api/";

        private const string Player = SERVER + "player/";
        private const string Pulsa = SERVER + "pulsa/";
        private const string App = SERVER + "app/";

        public const string Authenticate = Player + "authenticate";
        public const string Register = Player + "register";
        public const string ConfirmOTP = Player + "confirm_otp";
        public const string GetToken = Player + "get_token";
        public const string UpdateToken = Player + "update_token";

        public const string PriceList = Pulsa + "pricelist";
        public const string Purchase = Pulsa + "purchase";

        public const string LastVersion = App + "last_version";
        public const string PlayingGame = App + "playing_game";
    }
}
