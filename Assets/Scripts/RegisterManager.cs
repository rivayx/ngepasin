﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using SimpleJSON;

public class RegisterManager : MonoBehaviour
{
    [SerializeField] TMP_InputField InputNoHP;    
    [SerializeField] TMP_InputField InputOTP;

    [SerializeField] TextMeshProUGUI TextOTP;

    [SerializeField] GameObject PanelPhone;
    [SerializeField] GameObject PanelOTP;

    private void OnEnable()
    {
        transform.SetAsLastSibling();
        PanelPhone.SetActive(true);
        PanelOTP.SetActive(false);
    }

    public void SalahNomor()
    {
        PanelPhone.SetActive(true);
        PanelOTP.SetActive(false);
    }

    public void ConfirmOTP()
    {
        Singleton.Instance.Loading = true;
        PostData.ConfirmOTP(InputNoHP.text, InputOTP.text, JSONActionConfirmOTP);
    }

    private void JSONActionConfirmOTP(string response)
    {
        Singleton.Instance.Loading = false;
        Debug.Log("Response ConfirmOTP ::\n" + response);

        JSONNode node = JSON.Parse(response);
        int status_code = node["status_code"].AsInt;
        string message = node["message"];

        if (status_code == 100)
        {
            string apikey = node["apikey"];
            int token = node["token"].AsInt;

            Singleton.Instance.apiKey = apikey;
            Singleton.Instance.TotalToken = token;
            EncryptedPlayerPrefs.SetString("phone", InputNoHP.text);
            PopupMessage.Instance.OpenMessage("Berhasil", message, () => Destroy(this.gameObject), false);
        }
        else
        {
            PopupMessage.Instance.OpenMessage("Gagal", message);
        }
    }

    public void RequestOTP()
    {
        Singleton.Instance.Loading = true;
        PostData.Register(InputNoHP.text, JSONActionRegister);
    }

    private void JSONActionRegister(string response)
    {
        Singleton.Instance.Loading = false;
        Debug.Log("Response Register ::\n" + response);

        JSONNode node = JSON.Parse(response);
        int status_code = node["status_code"].AsInt;

        if (status_code == 100)
        {
            TextOTP.text = "Masukin kode OTP yang dikirim ke nomor WhatsApp <b>" + InputNoHP.text + "</b>.";
            PanelPhone.SetActive(false);
            PanelOTP.SetActive(true);
        }
    }
}
