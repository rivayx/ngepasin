﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    public int score { get; set; }

    [SerializeField]
    protected TextMeshProUGUI scoreText;

    void Awake()
    {
        score = 0;
    }

    public virtual void IncrementScore()
    {
        scoreText.text = (++score).ToString();

        FindObjectOfType<TokenManager>().IncrementTokenFromScore(score);
    }
}
