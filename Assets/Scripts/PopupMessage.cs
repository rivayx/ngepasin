﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using TMPro;

public class PopupMessage : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI Tittle;
    [SerializeField] TextMeshProUGUI Message;

    [SerializeField] Button OKButton;
    [SerializeField] Button CancelButton;

    Transform Background;

    public static PopupMessage Instance
    {
        get
        {
            if (FindObjectOfType<PopupMessage>())
            {
                return FindObjectOfType<PopupMessage>();
            }
            else
            {
                GameObject go = Instantiate(Singleton.Instance.popupMessage.gameObject, FindObjectOfType<Canvas>().transform);
                return go.GetComponent<PopupMessage>();
            }

        }
    }

    private void Awake()
    {
        Background = Tittle.transform.parent;
    }

    private void OnEnable()
    {
        transform.SetAsLastSibling();

        OKButton.gameObject.SetActive(true);
        CancelButton.gameObject.SetActive(false);

        Background.localScale = Vector3.zero;

        OKButton.onClick.AddListener(() => Destroy(gameObject));
        CancelButton.onClick.AddListener(() => Destroy(gameObject));
    }

    private void Update()
    {
        Background.localScale = Vector3.Lerp(Background.localScale, Vector3.one, Time.deltaTime * 10);
    }

    public void OpenMessage(string tittle, string message)
    {
        Tittle.text = tittle;
        Message.text = message;
    }

    public void OpenMessage(string tittle, string message, UnityAction okAction)
    {
        Tittle.text = tittle;
        Message.text = message;
        OKButton.onClick.AddListener(okAction);
    }

    public void OpenMessage(string tittle, string message, UnityAction okAction, UnityAction cancelAction)
    {
        Tittle.text = tittle;
        CancelButton.gameObject.SetActive(true);
        Message.text = message;
        OKButton.onClick.AddListener(okAction);
        CancelButton.onClick.AddListener(cancelAction);
    }

    public void OpenMessage(string tittle, string message, UnityAction okAction, bool cancel)
    {
        Tittle.text = tittle;
        CancelButton.gameObject.SetActive(cancel);
        Message.text = message;
        OKButton.onClick.AddListener(okAction);
    }
}
