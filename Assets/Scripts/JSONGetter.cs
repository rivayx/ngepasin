﻿using System;
using System.Text.RegularExpressions;
using BaseApps;
using UnityEngine;
using UnityEngine.Events;


public class JSONGetter : MonoBehaviour
{
    private WWW _URLLink;
    private JSONEvent _JSONEventCaller;

    public static JSONGetter GetJSON()
    {
        GameObject game = new GameObject("JSONGetter");
        game.AddComponent<JSONGetter>();

        JSONGetter getter = game.GetComponent<JSONGetter>() as JSONGetter;
        return getter;
    }

    public void StartParsing(string urlLink, UnityAction<string> unityAction)
    {
        name = urlLink;
        name = Regex.Replace(name, URL.SERVER, string.Empty);
        _URLLink = new WWW(urlLink);

        EventJSON(unityAction);
    }

    public void StartParsing(string urlLink, WWWForm form, UnityAction<string> unityAction)
    {
        name = urlLink;
        name = Regex.Replace(name, URL.SERVER, string.Empty);
        _URLLink = new WWW(urlLink, form);

        EventJSON(unityAction);
    }

    private void EventJSON(UnityAction<string> unityAction)
    {
        _JSONEventCaller = new JSONEvent();
        _JSONEventCaller.AddListener(unityAction);
    }

    void FixedUpdate()
    {
        if (_URLLink.isDone)
        {
            _JSONEventCaller.Invoke(_URLLink.text);

            if (_URLLink.error != null)
            {
                Debug.LogError(_URLLink.url + " Error :: \n" + _URLLink.error);
            }
            Destroy(gameObject);
        }
    }

    void OnDestroy()
    {
        _JSONEventCaller.RemoveAllListeners();
        _JSONEventCaller = null;
        _URLLink = null;
    }
}