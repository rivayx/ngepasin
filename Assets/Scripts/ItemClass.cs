﻿using BaseApps;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemClass { }

public class ProductListItemClass : ItemClass
{
    public string Code { get; set; }
    public string Name { get; set; }
    public int HargaToken { get; set; }
    public bool Status { get; set; }

    public ProductListItemClass(string code, string name, int harga_token, bool status)
    {
        Code = code;
        Name = name;
        HargaToken = harga_token;
        Status = status;
    }
}