﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;
using SimpleJSON;

public class HomeManager : MonoBehaviour
{
    [SerializeField] Button ButtonPulsa;

    [SerializeField] GameObject PanelPulsa;
    [SerializeField] GameObject PanelGame;

    [Header("Variable GamePanel")]
    [SerializeField] Transform HolderGameList;


    [Header("Variable PulsaPanel")]
    [SerializeField] TMP_InputField PhoneNumber;
    [SerializeField] Image ProviderImage;
    [SerializeField] Transform ProductItem;


    private List<GameObject> ProductListGameObject = new List<GameObject>();


    private enum PanelActive { game, pulsa }
    PanelActive panelActive;

    float OpenTimeGamePanel;
    float CloseTimeGamePanel;

    bool gamePanelClosed;
    bool pulsaPanelClosed;

    private void Start()
    {
        CallAdmobAds();

        for (int i = 0; i < HolderGameList.childCount; i++)
        {
            HolderGameList.GetChild(i).transform.localScale = Vector2.zero;
        }
        PhoneNumber.text = "";
        ProductItem.gameObject.SetActive(false);
        panelActive = PanelActive.game;


        ClearProductList();
        PhoneNumber.onValueChanged.AddListener(GetPriceList);
        ButtonPulsa.onClick.AddListener(ButtonPulsaClick);
    }

    private void Update()
    {

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            PopupMessage.Instance.OpenMessage("Keluar Aplikasi", "Beneran mau keluar?", Application.Quit, true);
        }

        switch (panelActive)
        {
            case PanelActive.game:

                PanelGame.SetActive(true);
                PulsaPanelClosing();

                if (pulsaPanelClosed)
                {
                    PanelPulsa.SetActive(false);
                    ButtonPulsa.GetComponentInChildren<TextMeshProUGUI>().text = "Punya Token? Sini tuker sama kuota";
                    GamePanelOpening();
                }

                break;

            case PanelActive.pulsa:

                PanelPulsa.SetActive(true);
                GamePanelClosing();

                if (gamePanelClosed)
                {
                    PanelGame.SetActive(false);
                    ButtonPulsa.GetComponentInChildren<TextMeshProUGUI>().text = "Tokennya kurang? Dapetinnya disini";
                    PulsaPanelOpening();
                }
                
                break;
        }
    }

    private void GamePanelOpening()
    {
        for (int i = 0; i < HolderGameList.childCount; i++)
        {
            if (OpenTimeGamePanel + (0.08 * i) < Time.fixedTime)
                HolderGameList.GetChild(i).transform.localScale = Vector2.Lerp(HolderGameList.GetChild(i).transform.localScale, Vector2.one, Time.deltaTime * 8);

            if (HolderGameList.GetChild(i).GetComponent<Animation>())
                HolderGameList.GetChild(i).GetComponent<Animation>().Play();
        }
    }

    private void GamePanelClosing()
    {
        for (int i = 0; i < HolderGameList.childCount; i++)
        {
            if (CloseTimeGamePanel + (0.08 * i) < Time.fixedTime)
                HolderGameList.GetChild(i).transform.localScale = Vector2.Lerp(HolderGameList.GetChild(i).transform.localScale, Vector2.zero, Time.deltaTime * 8);

            if (HolderGameList.GetChild(i).GetComponent<Animation>())
                HolderGameList.GetChild(i).GetComponent<Animation>().Stop();
        }
        if (HolderGameList.GetChild(HolderGameList.childCount - 1).transform.localScale.x < .5f)
            gamePanelClosed = true;
    }

    private void PulsaPanelOpening()
    {
        PhoneNumber.transform.localScale = Vector2.Lerp(PhoneNumber.transform.localScale, Vector2.one, Time.deltaTime * 8);
        
    }

    private void PulsaPanelClosing()
    {
        PhoneNumber.transform.localScale = Vector2.Lerp(PhoneNumber.transform.localScale, Vector2.zero, Time.deltaTime * 8);

        if (PhoneNumber.transform.localScale.x < .1f)
            pulsaPanelClosed = true;
    }

    private void ButtonPulsaClick()
    {
        switch (panelActive)
        {
            case PanelActive.game:

                PhoneNumber.transform.localScale = Vector2.zero;

                pulsaPanelClosed = false;
                panelActive = PanelActive.pulsa;
                CloseTimeGamePanel = Time.fixedTime;
                break;

            case PanelActive.pulsa:
                for (int i = 0; i < HolderGameList.childCount; i++)
                {
                    HolderGameList.GetChild(i).transform.localScale = Vector2.zero;
                }

                ClearProductList();
                gamePanelClosed = false;
                panelActive = PanelActive.game;
                OpenTimeGamePanel = Time.fixedTime;
                break;
        }
    }



    #region Proses Pulsa

    bool getingPricelist;
    private void GetPriceList(string value)
    {
        if (value.Length >= 4)
        {
            if (!getingPricelist)
            {
                if (ProductListGameObject.Count == 0)
                {
                    string prefix = value.Substring(0, 4);
                    PostData.PriceList(prefix, JSONActionGetPriceList);
                    getingPricelist = true;
                }
            }
        }
        else
        {
            ClearProductList();
            ProviderImage.enabled = false;
            getingPricelist = false;
        }
    }

    private void JSONActionGetPriceList(string response)
    {
        Debug.Log("Response GetPriceList ::\n" + response);

        JSONNode node = JSON.Parse(response);

        string status = node["status"];

        if (status == "failed")
        {
            string message = node["message"];
            PopupMessage.Instance.OpenMessage("Gangguan", message);
        }
        else
        {
            string provider = node["provider"];
            if (!string.IsNullOrEmpty(provider))
            {
                Debug.Log(provider);
                Texture2D tex = Resources.Load("provider/" + provider) as Texture2D;
                ProviderImage.enabled = true;
                ProviderImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));

                GetProductList productList = new GetProductList(response);

                StartCoroutine(CreateDynamicProduct(productList.ParseJSON()));
            }
            else
            {
                PopupMessage.Instance.OpenMessage("Nomornya Salah", "Coba cek lagi nomornya dah. Gak ada operator yang cocok.", () => PhoneNumber.onFocusSelectAll = true);
            }
        }
    }

    private IEnumerator CreateDynamicProduct(List<ItemClass> productList)
    {
        foreach (ProductListItemClass product in productList)
        {
            GameObject item = Instantiate(ProductItem.gameObject, ProductItem.parent);
            item.name = product.Code + " - " + product.Name;
            item.SetActive(true);

            ProductItemVariable itemVariable = item.GetComponent<ProductItemVariable>();
            itemVariable.ProductName.text = product.Name;
            itemVariable.HargaToken.text = product.HargaToken.ToString();
            itemVariable.ProductCode = product.Code;
            
            Button itemButton = item.GetComponent<Button>();
            itemButton.interactable = product.Status;
            itemButton.onClick.AddListener(() => ProductClick(product));

            ProductListGameObject.Add(item);

            yield return new WaitForEndOfFrame();
        }
    }

    private void ProductClick(ProductListItemClass product)
    {
        if (Singleton.Instance.TotalToken < product.HargaToken)
        {
            PopupMessage.Instance.OpenMessage(
                "Tokennya Kurang",
                "Aduh.. maap banget ini, Tokennya masih kurang " + (product.HargaToken - Singleton.Instance.TotalToken) +
                " kalo mau tuker sama \"" + product.Name + "\". Coba kumpulin lagi dulu dah ya kalo gak tuker sama yang cukup aja");
        }
        else
        {
            PopupMessage.Instance.OpenMessage(
                "Konfirmasi",
                "Beneran mau tuker token " + product.HargaToken + " sama " + product.Name + "?",
                () => PurchasePulsa(product.Code), true);
        }
    }

    private void PurchasePulsa(string code)
    {
        Singleton.Instance.Loading = true;
        PostData.Purchase(code, PhoneNumber.text, JSONActionPurchase);
    }

    private void JSONActionPurchase(string response)
    {
        Singleton.Instance.Loading = false;
        Debug.Log("Response Purchase ::\n" + response);

        JSONNode node = JSON.Parse(response);
        string status = node["status"];
        string message = node["message"];
        if (status == "success")
        {
            PopupMessage.Instance.OpenMessage("Berhasil", message);
            Singleton.Instance.TotalToken = node["sisa_token"].AsInt;
        }
        else
        {
            PopupMessage.Instance.OpenMessage("Gagal", message);
        }
    }

    private void ClearProductList()
    {
        if (ProductListGameObject.Count > 0)
        {
            getingPricelist = false;
            foreach (GameObject item in ProductListGameObject)
            {
                Destroy(item);
            }
            ProductListGameObject.Clear();
        }
    }

    #endregion



    public void CallAdmobAds()
    {
        FindObjectOfType<AdManager>().ShowAdmobBanner();
    }

    public void LoadScene(string sceneName)
    {
        ClearProductList();
        ButtonPulsa.onClick.RemoveAllListeners();
        PhoneNumber.onValueChanged.RemoveAllListeners();

        PostData.PlayingGame(sceneName, JSONActionPalyingGame);
        SceneManager.LoadSceneAsync(sceneName);
    }

    private void JSONActionPalyingGame(string response)
    {
        Debug.Log("Response PalyingGame ::\n" + response);
    }
}
