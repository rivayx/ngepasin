﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject startPanel, endPanel, muteImage, reviveButton;
    public TextMeshProUGUI scoreText, highScoreText, endScoreText, endHighScoreText;

    void Start()
    {
        Begin();
    }

    protected virtual void Begin()
    {
        CallAdmobAds();
    }

    private void CallAdmobAds()
    {
        FindObjectOfType<AdManager>().ShowAdmobBanner();
        if (Time.time != Time.timeSinceLevelLoad)
            FindObjectOfType<AdManager>().ShowAdmobInterstitial();
    }

    public void BackToHome()
    {
        Debug.Log("Back to Home");
        SceneManager.LoadScene(0, LoadSceneMode.Single);
    }
}
