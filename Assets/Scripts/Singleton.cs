﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SimpleJSON;

public class Singleton : MonoBehaviour
{
    public PopupMessage popupMessage;
    public RegisterManager Register;


    //[SerializeField] KeyCode ScreenshotTriger;

    [SerializeField] GameObject LoadingPrefab;
    GameObject LoadingPanel;
    public bool Loading { get; set; }

    [SerializeField] AudioSource tokenSound;
    public bool soundIsOn { get; set; }

    public string apiKey { get; set; }
    public int TotalToken { get; set; }

    private bool updateLetter = false;

    public static Singleton Instance { get; private set; }
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);

            TotalToken = 0;

            if (string.IsNullOrEmpty(EncryptedPlayerPrefs.GetString("phone")))            
                Instantiate(Register.gameObject, FindObjectOfType<Canvas>().transform);
            else
                PostData.Authenticate(EncryptedPlayerPrefs.GetString("phone"), JSONActionAuthenticate);

            if (!updateLetter)
                PostData.LastVersion(JSONActionLastUpdate);
        }
    }

    

    private void Start()
    {
        //PlayerPrefs.DeleteAll();
        soundIsOn = PlayerPrefs.GetInt("Audio") == 0;
        
    }

    private void Update()
    {
        //if (Input.GetKey(ScreenshotTriger))
        //{
        //    ScreenCapture.CaptureScreenshot(Application.persistentDataPath + "/" + Time.time+ ".png");
        //}


        if (soundIsOn)
        {
            if (PlayerPrefs.GetInt("Audio") == 1)
            {
                PlayerPrefs.SetInt("Audio", 0);
            }
        }
        else
        {
            if (PlayerPrefs.GetInt("Audio") == 0)
            {
                PlayerPrefs.SetInt("Audio", 1);
            }
        }

        if (Loading)
        {
            if (LoadingPanel == null)
                LoadingPanel = Instantiate(LoadingPrefab, FindObjectOfType<Canvas>().transform);

            LoadingPanel.transform.SetAsLastSibling();
        }
        else
        {
            Destroy(LoadingPanel);
            LoadingPanel = null;
        }
    }

    private void JSONActionLastUpdate(string response)
    {
        Debug.Log("Response LastUpdate ::\n" + response);

        JSONNode node = JSON.Parse(response);
        string version_code = node["version_code"];
        string force_update = node["force_update"];

        if (version_code != Application.version)
        {
            if (force_update == "true")
            {
                PopupMessage.Instance.OpenMessage("Eh ada yang baru",
                    "Update terbaru udah ada nih. Yok ah di Update dulu, biar makin asik mainnya",
                    () => Application.OpenURL("market://details?id=" + Application.identifier));
            }
            else
            {
                PopupMessage.Instance.OpenMessage("Eh ada yang baru",
                    "Update terbaru udah ada nih. Yok ah di Update dulu, biar makin asik mainnya",
                    () => Application.OpenURL("market://details?id=" + Application.identifier),
                    () => updateLetter = true);
            }
        }
    }

    private void JSONActionAuthenticate(string response)
    {
        Debug.Log("Response Authenticate ::\n" + response);

        JSONNode node = JSON.Parse(response);
        apiKey = node["apikey"];
        TotalToken = node["token"].AsInt;
    }

    public void TokenSound()
    {
        if (soundIsOn)
            tokenSound.Play();
    }
}
