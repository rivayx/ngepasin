﻿using System;
using UnityEngine;
using System.Collections.Generic;
using SimpleJSON;

public class GetProductList : JSONParsing
{
    public GetProductList(string JSONPath) : base(JSONPath) { }

    protected override List<ItemClass> JSONObjectToItems(JSONNode jsonNode)
    {
        List<ItemClass> itemClass = new List<ItemClass>();

        for (int i = 0; i < jsonNode["pricelist"].Count; i++)
        {
            JSONNode node = jsonNode["pricelist"][i];

            string code = node["code"];
            string name = node["nama"];
            int harga_token = node["harga_token"].AsInt;
            string status = node["status"];
            bool status_bool = (status == "Active");
           

            ItemClass item = new ProductListItemClass(code, name, harga_token, status_bool);
            itemClass.Add(item);
        }
        return itemClass;
    }
}