﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using SimpleJSON;

public class TokenManager : MonoBehaviour
{
    [SerializeField]
    protected int KelipatanScoreUntukDapatToken;

    protected int TokenInGame = 0;
    private int CurrentToken;

    void Awake()
    {
        TokenInGame = 0;
        if (Singleton.Instance != null)
            CurrentToken = Singleton.Instance.TotalToken;
    }

    public void IncrementTokenFromScore(int score)
    {
        if (KelipatanScoreUntukDapatToken > 0 && score > 0)
        {
            if (score % KelipatanScoreUntukDapatToken == 0)
            {
                IncrementToken(1);
            }
        }
    }

    void Update()
    {
        if (GetComponentInChildren<TextMeshProUGUI>().text != Singleton.Instance.TotalToken.ToString())
            GetComponentInChildren<TextMeshProUGUI>().text = Singleton.Instance.TotalToken.ToString();
    }

    public void UpdateToken()
    {
        if (CurrentToken + TokenInGame == Singleton.Instance.TotalToken)
        {
            PostData.UpdateToken(JSONActionSetToken);
        }
        else
        {
            Debug.Log("Token salah");
            Singleton.Instance.TotalToken = CurrentToken;
        }
    }

    private void JSONActionSetToken(string response)
    {
        Debug.Log("Response SetToken ::\n" + response);

        JSONNode node = JSON.Parse(response);
        Singleton.Instance.TotalToken = node["token"].AsInt;
    }

    public void IncrementToken(int countOfToken)
    {
        TokenInGame += countOfToken;
        Singleton.Instance.TotalToken += countOfToken;
        Singleton.Instance.TokenSound();
    }
}
