﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BaseApps;
using UnityEngine.Events;

public class PostData
{
    public static void LastVersion(UnityAction<string> unityAction)
    {
        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.LastVersion, unityAction);

        return;
    }

    public static void PlayingGame(string game_name, UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("apikey", Singleton.Instance.apiKey);
        form.AddField("game_name", game_name);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.PlayingGame, form, unityAction);

        return;
    }

    public static void Authenticate(string phone, UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("phone", phone);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.Authenticate, form, unityAction);
    }

    public static void Register(string phone, UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("phone", phone);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.Register, form, unityAction);
    }

    public static void ConfirmOTP(string phone, string otp, UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("phone", phone);
        form.AddField("otp", otp);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.ConfirmOTP, form, unityAction);
        return;
    }

    public static void GetToken(UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("apikey", Singleton.Instance.apiKey);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.UpdateToken, form, unityAction);
        return;
    }

    public static void UpdateToken(UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("apikey", Singleton.Instance.apiKey);
        form.AddField("token", Singleton.Instance.TotalToken);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.UpdateToken, form, unityAction);
        return;
    }

    public static void PriceList(string prefix, UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("prefix", prefix);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.PriceList, form, unityAction);
        return;
    }

    public static void Purchase(string code, string phone_number, UnityAction<string> unityAction)
    {
        WWWForm form = new WWWForm();
        form.AddField("apikey", Singleton.Instance.apiKey);
        form.AddField("code", code);
        form.AddField("phone_number", phone_number);

        JSONGetter getJSON = JSONGetter.GetJSON();
        getJSON.StartParsing(URL.Purchase, form, unityAction);
        return;
    }
}
