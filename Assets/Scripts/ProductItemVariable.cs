﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ProductItemVariable : MonoBehaviour
{
    public TextMeshProUGUI ProductName;
    public TextMeshProUGUI HargaToken;
    public string ProductCode { get; set; }
}
