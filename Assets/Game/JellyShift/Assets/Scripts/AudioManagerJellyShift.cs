﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerJellyShift : MonoBehaviour {

    //------------------------CREDITS----------------------------
    //Background music by Eric Matyas: http://www.soundimage.org
    //Sound effects: https://www.noiseforfun.com
    //-----------------------------------------------------------

    [SerializeField]
    private AudioSource backgroundMusic, scoreSound, deathSound, buttonClickSound, skinSwitchSound, notEnoughTokenSound;

    //Functions are called by other scripts when it is necessary

    public void StopBackgroundMusic()
    {
        backgroundMusic.Stop();
    }

    public void PlayBackgroundMusic()
    {
        if (Singleton.Instance.soundIsOn)
            backgroundMusic.Play();
    }

    public void ScoreSound()
    {
        if (Singleton.Instance.soundIsOn)
            scoreSound.Play();
    }

    public void DeathSound()
    {
        if (Singleton.Instance.soundIsOn)
            deathSound.Play();
    }

    public void ButtonClickSound()
    {
        if (Singleton.Instance.soundIsOn)
            buttonClickSound.Play();
    }

    public void NotEnoughTokenSound()
    {
        if (Singleton.Instance.soundIsOn)
            notEnoughTokenSound.Play();
    }

    public void SkinSwitchSound()
    {
        if (Singleton.Instance.soundIsOn)
            skinSwitchSound.Play();
    }
}
