﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreManagerJellyShift : ScoreManager
{
    private Animation cameraAnim;

    void Start()
    {
        cameraAnim = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animation>(); 
    }

    public override void IncrementScore()
    {
        base.IncrementScore();

        FindObjectOfType<AudioManagerJellyShift>().ScoreSound();      //Plays scoreSound
        cameraAnim.Play();      //Plays camera animation

        FindObjectOfType<TokenManager>().IncrementTokenFromScore(score);
    }
}
