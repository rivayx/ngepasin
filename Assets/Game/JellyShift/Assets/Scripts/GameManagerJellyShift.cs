﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManagerJellyShift : GameManager 
{
    public GameObject skinsPanel, pausedPanel, pauseButton;
    public Animation cameraAnim;

    [HideInInspector]
    public bool gameIsOver = false, gameHasStarted = false;

    protected override void Begin()
    {
        base.Begin();
        StartPanelActivation();
        HighScoreCheck();
        AudioCheck();
        CheckCamera();
    }

    public void Initialize()
    {
        scoreText.enabled = false;
        pauseButton.SetActive(false);
        FindObjectOfType<PlayerScaleJellyShift>().enabled = false;
    }

    public void StartPanelActivation()
    {
        Initialize();
        startPanel.SetActive(true);
        skinsPanel.SetActive(false);
        endPanel.SetActive(false);
        pausedPanel.SetActive(false);
    }

    public void SkinsPanelActivation()
    {
        startPanel.SetActive(false);
        skinsPanel.SetActive(true);
        pausedPanel.SetActive(false);
    }

    public void SkinsBackButton()
    {
        StartPanelActivation();
        FindObjectOfType<AudioManagerJellyShift>().ButtonClickSound();
    }

    public void EndPanelActivation()
    {
        StopObstacles();

        gameIsOver = true;
        FindObjectOfType<AudioManagerJellyShift>().DeathSound();
        FindObjectOfType<PlayerScaleJellyShift>().enabled = false;
        FindObjectOfType<SpawnerJellyShift>().CancelInvoke();
        startPanel.SetActive(false);
        endPanel.SetActive(true);
        pausedPanel.SetActive(false);
        skinsPanel.SetActive(false);
        scoreText.enabled = false;
        endScoreText.text = scoreText.text;
        pauseButton.SetActive(false);
        HighScoreCheck();

        FindObjectOfType<TokenManager>().UpdateToken();
    }

    public void StopObstacles()
    {
        foreach (GameObject obst in GameObject.FindGameObjectsWithTag("ObstacleHolder"))
            obst.GetComponent<ObstacleHolderJellyShift>().StopObstacle();
    }

    public void MoveObstacles()
    {
        foreach (GameObject obst in GameObject.FindGameObjectsWithTag("ObstacleHolder"))
            obst.GetComponent<ObstacleHolderJellyShift>().MoveObstacle();
    }

    public void PausedPanelActivation()
    {
        startPanel.SetActive(false);
        endPanel.SetActive(false);
        pausedPanel.SetActive(true);
    }

    public void HighScoreCheck()
    {
        if (FindObjectOfType<ScoreManagerJellyShift>().score > PlayerPrefs.GetInt("HighScoreJellyShift", 0))
        {
            PlayerPrefs.SetInt("HighScoreJellyShift", FindObjectOfType<ScoreManagerJellyShift>().score);
        }
        highScoreText.text = "Paling Gede " + PlayerPrefs.GetInt("HighScoreJellyShift", 0).ToString();
        endHighScoreText.text = "Paling Gede " + PlayerPrefs.GetInt("HighScoreJellyShift", 0).ToString();
    }

    public void AudioCheck()
    {
        if (Singleton.Instance.soundIsOn)
        {
            muteImage.SetActive(false);
            FindObjectOfType<AudioManagerJellyShift>().PlayBackgroundMusic();
        }
        else
        {
            muteImage.SetActive(true);
            FindObjectOfType<AudioManagerJellyShift>().StopBackgroundMusic();
        }
    }

    public void StartButton()
    {
        gameHasStarted = true;
        pauseButton.SetActive(true);
        scoreText.enabled = true;
        startPanel.SetActive(false);
        FindObjectOfType<AudioManagerJellyShift>().ButtonClickSound();
        FindObjectOfType<PlayerScaleJellyShift>().enabled = true;
        FindObjectOfType<SpawnerJellyShift>().Spawn();
    }

    public void RestartButton()
    {
        FindObjectOfType<AudioManagerJellyShift>().ButtonClickSound();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AudioButton()
    {
        FindObjectOfType<AudioManagerJellyShift>().ButtonClickSound();

        Singleton.Instance.soundIsOn = !Singleton.Instance.soundIsOn;
        AudioCheck();
    }

    public void PauseButton()
    {
        pauseButton.SetActive(false);
        PausedPanelActivation();
        scoreText.enabled = false;
        FindObjectOfType<AudioManagerJellyShift>().StopBackgroundMusic();
        Time.timeScale = 0f;
    }

    public void ResumeButton()
    {
        Time.timeScale = 1f;
        FindObjectOfType<AudioManagerJellyShift>().PlayBackgroundMusic();
        scoreText.enabled = true;
        pauseButton.SetActive(true);
        pausedPanel.SetActive(false);
    }

    public void HomeButton()
    {
        ResumeButton();
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void Revive()
    {
        FindObjectOfType<AdManager>().ShowAdmobRewardVideo();       //Shows Admob Reward Video ad

        gameIsOver = false;

        endPanel.SetActive(false);
        reviveButton.SetActive(false);
        pauseButton.SetActive(true);
        scoreText.enabled = true;
        FindObjectOfType<PlayerScaleJellyShift>().enabled = true;
        MoveObstacles();
        FindObjectOfType<SpawnerJellyShift>().Invoke("Spawn", 1f);
        FindObjectOfType<CollisionJellyShift>().Invoke("CanCollideAgain", 0.5f);
    }

    public void CameraButton()
    {
        if(PlayerPrefs.GetInt("CameraJellyShift", 0) == 0)
        {
            cameraAnim.Play("MiddleViewAnim");
            PlayerPrefs.SetInt("CameraJellyShift", 1);
        }
        else
        {
            cameraAnim.Play("SideViewAnim");
            PlayerPrefs.SetInt("CameraJellyShift", 0);
        }
    }

    public void CheckCamera()
    {
        if (PlayerPrefs.GetInt("CameraJellyShift", 0) == 0)
            cameraAnim.Play("SideViewAnim");
        else
            cameraAnim.Play("MiddleViewAnim");
    }
}
