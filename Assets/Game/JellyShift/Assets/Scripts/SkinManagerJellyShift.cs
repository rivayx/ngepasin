﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SkinManagerJellyShift : MonoBehaviour {

    public GameObject[] skins;

    public void ChangeSkin(int i)
    {
        Vector3 playerPos = GameObject.FindGameObjectWithTag("Player").transform.position;
        Destroy(GameObject.FindGameObjectWithTag("Player"));
        Instantiate(skins[i], playerPos, Quaternion.identity);
    }
}
