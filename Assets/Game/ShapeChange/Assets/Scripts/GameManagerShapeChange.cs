﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManagerShapeChange : GameManager
{
    public GameObject spawner;

    public GameObject jumpLeftButton, jumpRightButton, background;
    public Animation[] groundAnimations;
    public bool modeIsNormal = true;

    private Color[] colors = new Color[3] { Color.red, Color.blue, Color.green };


    protected override void Begin()
    {
        base.Begin();

        StartPanelActivation();
        CheckMode();
        HighScoreCheck();
        AudioCheck();
    }

    public void Initialize()
    {
        FindObjectOfType<SpawnerShapeChange>().enabled = false;
        scoreText.enabled = false;
    }

    public void StartPanelActivation()
    {
        startPanel.SetActive(true);
        endPanel.SetActive(false);
    }

    public void EndPanelActivation()
    {
        startPanel.SetActive(false);
        endPanel.SetActive(true);
        jumpRightButton.SetActive(false);
        jumpLeftButton.SetActive(false);
        scoreText.enabled = false;
        endScoreText.text = scoreText.text;
        HighScoreCheck();

        FindObjectOfType<TokenManager>().UpdateToken();
    }


    public void HighScoreCheck()
    {
        if (FindObjectOfType<ScoreManagerShapeChange>().score > PlayerPrefs.GetInt("HighScoreShapeChange", 0))
        {
            PlayerPrefs.SetInt("HighScoreShapeChange", FindObjectOfType<ScoreManagerShapeChange>().score);
        }
        highScoreText.text = "Paling Gede " + PlayerPrefs.GetInt("HighScoreShapeChange", 0).ToString();
        endHighScoreText.text = "Paling Gede " + PlayerPrefs.GetInt("HighScoreShapeChange", 0).ToString();
    }

    public void AudioCheck()
    {
        if (Singleton.Instance.soundIsOn)
        {
            muteImage.SetActive(false);
            FindObjectOfType<AudioManagerShapeChange>().PlayBackgroundMusic();
        }
        else
        {
            muteImage.SetActive(true);
            FindObjectOfType<AudioManagerShapeChange>().StopBackgroundMusic();
        }
    }

    public void StartButton()
    {
        spawner.SetActive(true);

        FindObjectOfType<JumpManagerShapeChange>().enabled = true;
        FindObjectOfType<SpawnerShapeChange>().enabled = true;
        scoreText.enabled = true;
        startPanel.SetActive(false);
        jumpLeftButton.SetActive(true);
        jumpRightButton.SetActive(true);
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void SkinsBackButton()
    {
        StartPanelActivation();
    }

    public void AudioButton()
    {
        Singleton.Instance.soundIsOn = !Singleton.Instance.soundIsOn;
        AudioCheck();
    }

    public void ChangeModeButton()
    {
        if (PlayerPrefs.GetInt("ModeShapeChange", 0) == 0)
            PlayerPrefs.SetInt("ModeShapeChange", 1);
        else
            PlayerPrefs.SetInt("ModeShapeChange", 0);
        CheckMode();
    }

    public void CheckMode()
    {
        if (PlayerPrefs.GetInt("ModeShapeChange", 0) == 0)
        {
            modeIsNormal = true;

            for (int i = 0; i < background.transform.childCount; i++)
                background.transform.GetChild(i).GetComponent<Renderer>().material.color = Color.white;

            for (int i = 0; i < groundAnimations.Length; i++)
            {
                groundAnimations[i].Stop();
                groundAnimations[i].Play("SameGround");
            }
        }
        else
        {
            modeIsNormal = false;

            for (int i = 0; i < background.transform.childCount; i++)
                background.transform.GetChild(i).GetComponent<Renderer>().material.color = colors[Random.Range(0, colors.Length)];

            for (int i = 0; i < groundAnimations.Length; i++)
                groundAnimations[i].Play();
        }
    }
}
