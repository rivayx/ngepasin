﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleMovementShapeChange : MonoBehaviour {

    public float movementSpeed;

    private Rigidbody rb;

	void Start () {
        rb = GetComponent<Rigidbody>();
        rb.AddForce(transform.forward * -movementSpeed);        //Moves the obstacle towards the player

        if (CompareTag("Cube Instance"))        //If the obstacle is cube
            transform.Rotate(Vector3.right, 90f);       //Then rotates it
        else if(CompareTag("Prism Instance"))       //If the obstacle is Prism
            transform.Rotate(Vector3.right, -90f);      //Then rotates it as well
	}
	
	void Update () {
        if (FindObjectOfType<CollisionShapeChange>().gameIsOver)       //Checks if the player collided with a collider which has different shape
            rb.Sleep();     //If it did, then stops the obstacle
	}
}
