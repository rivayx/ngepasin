﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerShapeChange : MonoBehaviour {

    //------------------------CREDITS----------------------------
    //Background music by Eric Matyas: http://www.soundimage.org
    //Sound effects: https://www.noiseforfun.com
    //-----------------------------------------------------------

    public AudioSource backgroundMusic, scoreSound, deathSound, colorChangeSound, skinSwitchSound, notEnoughTokenSound;

    //Functions are called when it is necessary

    public void StopBackgroundMusic()
    {
        backgroundMusic.Stop();
    }

    public void PlayBackgroundMusic()
    {
        if (Singleton.Instance.soundIsOn)
            backgroundMusic.Play();
    }

    public void ScoreSound()
    {
        if (Singleton.Instance.soundIsOn)
            scoreSound.Play();
    }

    public void DeathSound()
    {
        if (Singleton.Instance.soundIsOn)
            deathSound.Play();
    }

    public void ColorChangeSound()
    {
        if (Singleton.Instance.soundIsOn)
            colorChangeSound.Play();
    }

    public void NotEnoughTokenSound()
    {
        if (Singleton.Instance.soundIsOn)
            notEnoughTokenSound.Play();
    }

    public void SkinSwitchSound()
    {
        if (Singleton.Instance.soundIsOn)
            skinSwitchSound.Play();
    }
}
