﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionShapeChange : MonoBehaviour {

    public Mesh[] meshes;
    public GameObject collisionParticle, tokenParticle;

    private Color[] colors = new Color[3] { Color.red, Color.blue, Color.green };
    private ParticleSystemRenderer particleRen;
    private TrailRenderer trailRen;
    private Renderer playerRen;
    private MeshFilter playerMesh;


    [HideInInspector]
    public bool gameIsOver = false;

	// Use this for initialization
	void Start () {
        particleRen = GetComponent<ParticleSystemRenderer>();
        trailRen = GetComponent<TrailRenderer>();
        playerRen = GetComponent<Renderer>();
        playerMesh = GetComponent<MeshFilter>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cube Instance") || other.CompareTag("Sphere Instance") || other.CompareTag("Prism Instance"))        //If player collides with an obstacle
        {
            if (other.CompareTag(GetComponent<MeshFilter>().mesh.name))      //If the collided gameObject has the same mesh as the player
            {
                if(!(FindObjectOfType<GameManagerShapeChange>().modeIsNormal))
                    particleRen.material.color = trailRen.material.color = playerRen.material.color = colors[Random.Range(0, colors.Length)];

                FindObjectOfType<Camera>().GetComponent<Animation>().Play();
                playerMesh.mesh = meshes[Random.Range(0, meshes.Length)];       //Changes the player's mesh
                FindObjectOfType<ScoreManagerShapeChange>().IncrementScore();      //Increments score
                FindObjectOfType<AudioManagerShapeChange>().ScoreSound();      //Plays 'scoreSound'
            }
            else        //If the tags are different
            {
                gameIsOver = true;      //Game is over
                FindObjectOfType<AudioManagerShapeChange>().DeathSound();      //Plays 'deathSound'
                particleRen.enabled = trailRen.enabled = playerRen.enabled = false;
                GetComponent<Collider>().enabled = false;
                FindObjectOfType<JumpManagerShapeChange>().enabled = false;
                FindObjectOfType<GameManagerShapeChange>().EndPanelActivation();
            }
            GameObject tempCollisionParticle = Instantiate(collisionParticle, transform.position, Quaternion.identity);     //Instantiates a collisionParticle to the player's position
            tempCollisionParticle.GetComponent<Renderer>().material.color = playerRen.material.color;       //Sets its color identical to the player's color
            Destroy(tempCollisionParticle, 1.2f);       //Destroys it after x seconds
        }
        else if (other.CompareTag("Token"))
        {
            FindObjectOfType<TokenManager>().IncrementToken(1);
            Destroy(Instantiate(tokenParticle, transform.position, Quaternion.identity), 1.2f);
            Destroy(other.gameObject);
        }
    }
}
