﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManagerShapeHavoc : GameManager
{
    protected override void Begin()
    {
        base.Begin();

        StartPanelActivation();
        HighScoreCheck();
        AudioCheck();
    }

    public void Initialize()
    {
        FindObjectOfType<SpawnerShapeHavoc>().enabled = false;
        scoreText.enabled = false;
    }

    public void StartPanelActivation()
    {
        startPanel.SetActive(true);
        endPanel.SetActive(false);
    }

    public void EndPanelActivation()
    {
        startPanel.SetActive(false);
        endPanel.SetActive(true);
        scoreText.enabled = false;
        endScoreText.text = scoreText.text;
        HighScoreCheck();

        FindObjectOfType<TokenManager>().UpdateToken();
    }

    public void SkinsPanelActivation()
    {
        startPanel.SetActive(false);
    }

    public void HighScoreCheck()
    {
        if (FindObjectOfType<ScoreManagerShapeHavoc>().score > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", FindObjectOfType<ScoreManagerShapeHavoc>().score);
        }
        highScoreText.text = "PALING GEDE " + PlayerPrefs.GetInt("HighScore", 0).ToString();
        endHighScoreText.text = "PALING GEDE " + PlayerPrefs.GetInt("HighScore", 0).ToString();
    }

    public void AudioCheck()
    {
        if (Singleton.Instance.soundIsOn)
        {
            muteImage.SetActive(false);
            FindObjectOfType<AudioManagerShapeHavoc>().PlayBackgroundMusic();
        }
        else
        {
            muteImage.SetActive(true);
            FindObjectOfType<AudioManagerShapeHavoc>().StopBackgroundMusic();
        }
    }

    public void StartButton()
    {
        FindObjectOfType<SpawnerShapeHavoc>().enabled = true;
        scoreText.enabled = true;
        startPanel.SetActive(false);
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AudioButton()
    {
        Singleton.Instance.soundIsOn = !Singleton.Instance.soundIsOn;
        AudioCheck();
    }
}
