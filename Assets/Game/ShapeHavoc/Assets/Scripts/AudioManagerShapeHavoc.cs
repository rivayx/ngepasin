﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioManagerShapeHavoc : MonoBehaviour
{

    //------------------------CREDITS----------------------------
    //Background music by Eric Matyas: http://www.soundimage.org
    //Sound effects: https://www.noiseforfun.com
    //-----------------------------------------------------------

    [SerializeField]
    private AudioSource backgroundMusic, deathSound, scoreSound, havocSound;

    //Functions are called when it is necessary

    public void StopBackgroundMusic()
    {
        backgroundMusic.Stop();
    }

    public void PlayBackgroundMusic()
    {
        if (Singleton.Instance.soundIsOn)
            backgroundMusic.Play();
    }

    public void ScoreSound()
    {
        if (Singleton.Instance.soundIsOn)
            scoreSound.Play();
    }

    public void DeathSound()
    {
        if (Singleton.Instance.soundIsOn)
            deathSound.Play();
    }

    public void HavocSound()
    {
        if (Singleton.Instance.soundIsOn)
            havocSound.Play();
    }
}
