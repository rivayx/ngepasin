﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionShapeHavoc : MonoBehaviour {

    public GameObject destroyedParticle;
  
    void OnMouseDown()      //If the object has been clicked
    {
        if ((FindObjectOfType<PlayerShapeHavoc>().ammoCount > 0) && (!FindObjectOfType<PlayerShapeHavoc>().gameIsOver))     //If the player has ammo and the game is not over yet
        {
            FindObjectOfType<AudioManagerShapeHavoc>().HavocSound();      //Sound effect plays
            Destroy(Instantiate(destroyedParticle, transform.position, Quaternion.identity), 1f);       //Instantiates a particle and destroys it after x seconds
            Destroy(gameObject);        //Destroys the cube which has been clicked
            FindObjectOfType<PlayerShapeHavoc>().ammoCount--;     //Reduces ammo
        }
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))       //If gameobject collides with an obstacle, then game is over
        {
            //Game over functions
            FindObjectOfType<AudioManagerShapeHavoc>().DeathSound();
            Destroy(Instantiate(destroyedParticle, transform.position, Quaternion.identity), 1f);
            GetComponent<Animation>().Play("CubeDeathAnim");
            FindObjectOfType<PlayerShapeHavoc>().gameIsOver = true;
            FindObjectOfType<GameManagerShapeHavoc>().EndPanelActivation();
            GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Animation>().Play("CameraDeathAnim");
        }
    }
}
