﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovementShapeFit : MonoBehaviour {

    public float startMovementAfter = 4f;

    [HideInInspector]
    public bool isMovingRight = false;

    private int moved = 0, moveCount = 75;
    private Vector3 pos;

    void Start()
    {
        moveCount = 71;
        Invoke("MoveLeft", startMovementAfter);
    }

    public void MoveRight()
    {
        isMovingRight = true;
        pos = transform.position;
        pos.x += .25f;
        transform.position = pos;
        moved++;
        if (moved <= moveCount)
            Invoke("MoveRight", 0.03f);
        else
        {
            moved = 0;
            Invoke("MoveLeft", 0.03f);
        }
    }

    public void MoveLeft()
    {
        isMovingRight = false;
        pos = transform.position;
        pos.z += .25f;
        transform.position = pos;
        moved++;
        if (moved <= moveCount)
            Invoke("MoveLeft", 0.03f);
        else
        {
            moved = 0;
            Invoke("MoveRight", 0.03f);
        }
    }

    public void Stop()
    {
        CancelInvoke("MoveRight");
        CancelInvoke("MoveLeft");
    }
}
