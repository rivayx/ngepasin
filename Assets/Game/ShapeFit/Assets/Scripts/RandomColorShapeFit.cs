﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomColorShapeFit : MonoBehaviour
{
    public Color[] colors;

    void Start()
    {
        GetComponent<Renderer>().material.color = colors[Random.Range(0, colors.Length)];
    }

}
