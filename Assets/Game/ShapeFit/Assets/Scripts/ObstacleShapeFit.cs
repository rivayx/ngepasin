﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleShapeFit : MonoBehaviour {

    [HideInInspector]
    public bool moveOnX = false;

    [HideInInspector]
    public Color originalColor;

	void Start () {
        originalColor = GetComponent<Renderer>().material.color;        //Initializes the color of the gameObject
        if(tag == "Movable")        //If this is a movable obstacle
            GetComponent<Renderer>().material.color = Color.red;        //Then changes its color to red
	}
}
