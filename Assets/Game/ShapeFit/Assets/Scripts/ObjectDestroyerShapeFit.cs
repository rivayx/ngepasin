﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectDestroyerShapeFit : MonoBehaviour {

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Obstacle"))       //If gameObject collides with an obstacle
        {
            other.GetComponent<Animation>().Play("DeathAnim");      //Then plays its deathAnim
            Destroy(other.gameObject, 1.1f);        //And destroys after x secs
        }
        else if (!other.CompareTag("Finish"))
            Destroy(other.gameObject);      //If gameObject collides with anything but a gameObject with haas the tag "Finish", or "Obstacle" then destroys it immediately
    }
}
