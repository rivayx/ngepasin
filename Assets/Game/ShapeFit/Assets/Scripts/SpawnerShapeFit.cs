﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerShapeFit : MonoBehaviour {

    public GameObject obstacle;
    public GameObject[] shapes;
    public int obstacleFrequency = 8, nextObstacle = 5, firstStageAfter = 4, secondStageAfter = 8, thirdStageAfter = 14, fourthStageAfter = 20;

    [HideInInspector]
    public bool isMovingRight = false;

    private GameObject tempObstacle;
    private bool nextStage = false;
    private int moved = 0, moveCount = 70, countOfSpawns = 0;


    private Vector3 pos;

	void Start () {
        Spawn();        //First spawn
        MoveLeft();
        moveCount = 71;
	}

    public void MoveRight()
    {
        isMovingRight = true;
        pos = transform.position;
        pos.x += .25f;
        transform.position = pos;
        moved++;
        if(moved <= moveCount)
            Invoke("MoveRight", 0.03f);
        else
        {
            moved = 0;
            Invoke("MoveLeft", 0.03f);
        }
    }

    public void MoveLeft()
    {
        isMovingRight = false;
        pos = transform.position;
        pos.z += .25f;
        transform.position = pos;
        moved++;
        if (moved <= moveCount)
            Invoke("MoveLeft", 0.03f);
        else
        {
            moved = 0;
            Invoke("MoveRight", 0.03f);
        }
    }
    
    public void Stop()
    {
        //Stops the spawner
        CancelInvoke("MoveRight");
        CancelInvoke("MoveLeft");
    }

    void Update()
    {
        if (Vector3.Distance(tempObstacle.transform.position, transform.position) >= 2f)        //If the distance between the spawned obstacle and the spawner is bigger than x, then spawns a new obstacle
            Spawn();
    }

    public void Spawn()
    {
        tempObstacle = Instantiate(obstacle, transform.position, Quaternion.identity);       //Spawns a random obstacle to the spawner's position with the same rotation

        if (nextObstacle == 0)
        {
            GameObject tempShape = Instantiate(shapes[Random.Range(0, shapes.Length)], transform.position, Quaternion.identity);

            if (isMovingRight)
                tempShape.transform.Rotate(Vector3.up, 90f);

            nextObstacle = obstacleFrequency;
            countOfSpawns++;
            IncreaseDifficulty();       //If it is time, increases difficulty
        }
        else
            nextObstacle--;
    }

    public void IncreaseDifficulty()
    {
        if (countOfSpawns == firstStageAfter || countOfSpawns == secondStageAfter || countOfSpawns == thirdStageAfter || countOfSpawns == fourthStageAfter)     //First stage
            nextStage = true;

        if ((countOfSpawns >= firstStageAfter) && (countOfSpawns < secondStageAfter) && nextStage)      //Second stage
        {
            nextStage = false;
        }
        else if ((countOfSpawns >= secondStageAfter) && (countOfSpawns < thirdStageAfter) && nextStage)     //Third stage
        {
            nextStage = false;
            obstacleFrequency--;
        }
        else if ((countOfSpawns >= thirdStageAfter) && (countOfSpawns < fourthStageAfter) && nextStage)     //Fourth stage
        {
            nextStage = false;
            obstacleFrequency--;
        }
        else if ((countOfSpawns > fourthStageAfter) && nextStage)       //If it is time for the last stage
        {
            nextStage = false;
            obstacleFrequency--;
        }
    }
}
