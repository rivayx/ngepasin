﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManagerShapeFit : GameManager
{
    public GameObject changeShapeButton;

    [HideInInspector]
    public bool revived = false;

    protected override void Begin()
    {
        base.Begin();

        StartPanelActivation();
        HighScoreCheck();
        AudioCheck();
    }

    public void Initialize()
    {
        FindObjectOfType<PlayerMovementShapeFit>().enabled = false;
        changeShapeButton.SetActive(false);
        FindObjectOfType<SpawnerShapeFit>().enabled = false;
        scoreText.enabled = false;
    }

    public void StartPanelActivation()
    {
        startPanel.SetActive(true);
        endPanel.SetActive(false);
    }

    public void EndPanelActivation()
    {
        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Collider>().enabled = false;
        player.GetComponent<ParticleSystem>().Play();
        player.GetComponent<Renderer>().enabled = false;
        changeShapeButton.SetActive(false);
        FindObjectOfType<SpawnerShapeFit>().enabled = false;
        FindObjectOfType<SpawnerShapeFit>().Stop();
        FindObjectOfType<PlayerMovementShapeFit>().enabled = false;
        FindObjectOfType<PlayerMovementShapeFit>().Stop();
        startPanel.SetActive(false);
        endPanel.SetActive(true);
        scoreText.enabled = false;
        endScoreText.text = scoreText.text;
        HighScoreCheck();

        FindObjectOfType<TokenManager>().UpdateToken();
    }

    public void SkinsPanelActivation()
    {
        startPanel.SetActive(false);
    }

    public void HighScoreCheck()
    {
        if (FindObjectOfType<ScoreManagerShapeFit>().score > PlayerPrefs.GetInt("HighScoreShapeFit", 0))
        {
            PlayerPrefs.SetInt("HighScoreShapeFit", FindObjectOfType<ScoreManagerShapeFit>().score);
        }
        highScoreText.text = "PALING GEDE " + PlayerPrefs.GetInt("HighScoreShapeFit", 0).ToString();
        endHighScoreText.text = "PALING GEDE " + PlayerPrefs.GetInt("HighScoreShapeFit", 0).ToString();
    }

    public void AudioCheck()
    {
        if (Singleton.Instance.soundIsOn)
        {
            muteImage.SetActive(false);
            FindObjectOfType<AudioManagerShapeFit>().PlayBackgroundMusic();
        }
        else
        {
            muteImage.SetActive(true);
            FindObjectOfType<AudioManagerShapeFit>().StopBackgroundMusic();
        }
    }

    public void StartButton()
    {
        FindObjectOfType<PlayerMovementShapeFit>().enabled = true;
        FindObjectOfType<SpawnerShapeFit>().enabled = true;
        scoreText.enabled = true;
        startPanel.SetActive(false);
    }

    public void RestartButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void AudioButton()
    {
        Singleton.Instance.soundIsOn = !Singleton.Instance.soundIsOn;

        AudioCheck();
    }

    public void Revive()
    {
        FindObjectOfType<AdManager>().ShowAdmobRewardVideo();       //Shows Admob Reward Video ad

        revived = true;

        endPanel.SetActive(false);
        reviveButton.SetActive(false);


        GameObject player = GameObject.FindGameObjectWithTag("Player");
        player.GetComponent<Renderer>().enabled = true;
        changeShapeButton.SetActive(true);
        FindObjectOfType<SpawnerShapeFit>().enabled = true;
        FindObjectOfType<PlayerMovementShapeFit>().enabled = true;

        scoreText.enabled = true;
    }
}
