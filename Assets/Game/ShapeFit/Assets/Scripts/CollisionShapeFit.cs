﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionShapeFit : MonoBehaviour
{
    public Mesh[] meshes;
    public GameObject scoreParticle;

    private MeshFilter playerMeshFilter;
    private GameObject tempParticle;
    private int meshIndex = 0;

    void Start()
    {
        playerMeshFilter = GetComponent<MeshFilter>();
    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Cube Instance") || other.CompareTag("Prism Instance") || other.CompareTag("Sphere Instance"))     //If collides with an obstacle
        {
            if (other.CompareTag(playerMeshFilter.mesh.name))       //If the obstacle and the player have the same shape
            {
                tempParticle = Instantiate(scoreParticle, transform.position, Quaternion.identity);      //Istantiates a collision particle to the player's position
                tempParticle.GetComponent<ParticleSystemRenderer>().mesh = GetComponent<MeshFilter>().mesh;       //Sets particle's mesh identical to the player's mesh
                tempParticle.GetComponent<ParticleSystemRenderer>().material.color = other.GetComponent<Renderer>().material.color;       //Sets particle's mesh identical to the obstacle's color
                Destroy(tempParticle, 3f);       //Destroys particle after x seconds

                other.GetComponent<Animation>().Play("ShapeEndAnim");
                FindObjectOfType<ScoreManagerShapeFit>().IncrementScore();      //Increments score
                FindObjectOfType<AudioManagerShapeFit>().ScoreSound();      //Plays scoreSound
            }
            else     //If they have different shapes
            {
                FindObjectOfType<AudioManagerShapeFit>().DeathSound();      //Plays deathSound
                FindObjectOfType<GameManagerShapeFit>().EndPanelActivation();
            }
        }
    }

    public void ChangeMesh()
    {
        if (FindObjectOfType<GameManagerShapeFit>().revived)
        {
            FindObjectOfType<GameManagerShapeFit>().revived = false;

            if (FindObjectOfType<PlayerMovementShapeFit>().isMovingRight)
                FindObjectOfType<PlayerMovementShapeFit>().MoveRight();
            else
                FindObjectOfType<PlayerMovementShapeFit>().MoveLeft();

            if (FindObjectOfType<SpawnerShapeFit>().isMovingRight)
                FindObjectOfType<SpawnerShapeFit>().MoveRight();
            else
                FindObjectOfType<SpawnerShapeFit>().MoveLeft();


            FindObjectOfType<SpawnerShapeFit>().Spawn();
            Invoke("EnableCollider", 0.5f);
        }

        meshIndex++;
        if (meshIndex >= meshes.Length)
            meshIndex = 0;

        playerMeshFilter.mesh = meshes[meshIndex];
    }


    public void EnableCollider()
    {
        GetComponent<Collider>().enabled = true;
    }
}
